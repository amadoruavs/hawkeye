import keras
from keras.models import Model, load_model
from keras.layers import Input, Dense
from keras.optimizers import RMSprop
import numpy as np


# Create original model and save it
inputs = Input((1,))
dense_1 = Dense(10, activation='relu', name="DIS")(inputs)
dense_2 = Dense(10, activation='relu', name="DAT")(dense_1)
dense_3 = Dense(10, activation='relu', name="BOO")(dense_2)
dense_4 = Dense(10, activation='relu', name="BAT")(dense_3)
dense_5 = Dense(10, activation='relu', name="BOI")(dense_4)
dense_6 = Dense(10, activation='relu', name="BAH")(dense_5)
outputs = Dense(10)(dense_6)

model = Model(inputs=inputs, outputs=outputs)
model.compile(optimizer=RMSprop(), loss='mse')
model.save('test.h5')

def split_nn(model, split_points):
    networks = []
    first_model = Model(inputs=model.inputs, outputs=model.layers[split_points[0]-1].output)
    networks.append(first_model)
    split_points.append(len(model.layers))
    print(split_points)
    for i in range(0, len(split_points)-1):
        print("BROO")
        point = split_points[i]
        second_input = Input(model.layers[point].input_shape[1:])
        second_model = second_input
        for layer in model.layers[point:split_points[i+1]]:
            second_model = layer(second_model)
        second_model = Model(inputs=second_input, outputs=second_model)
        networks.append(second_model)
    return networks


for network in split_nn(model, [2,4]):
    network.summary()

# Please dont delete the stuff below, it might come in handy
'''
def split_nn(model, split_points):
    networks = []
    first_model = Model(inputs=model.inputs, outputs=model.layers[split_points[0]].output)
    networks.append(first_model)
    for point in split_points:
        point += 1
        second_input = Input(model.layers[point].input_shape[1:])
        second_model = second_input
        for layer in model.layers[point:]:
            second_model = layer(second_model)
        second_model = Model(inputs=second_input, outputs=second_model)
        networks.append(second_model)
    return networks

for network in split_nn(model, [2]):
    network.summary()
model.summary()
'''

# first_model = Model(inputs=model.inputs, outputs=model.layers[1].output)
# first_model.summary()


# second = 2
# second_input = Input(model.layers[second].input_shape[1:])
# second_model = second_input
# for layer in model.layers[second:]:
#     second_model = layer(second_model)
# second_model = Model(inputs=second_input, outputs=second_model)
# second_model.summary()

